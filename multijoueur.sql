-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jan 19, 2023 at 01:19 PM
-- Server version: 5.7.32
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `multijoueur`
--

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `datecreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `roomid` int(11) NOT NULL,
  `content` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `datecreated`, `roomid`, `content`) VALUES
(7, '2023-01-17 17:30:35', 2, 'bonjour ! ceci est un message 3'),
(8, '2023-01-17 17:30:40', 2, 'bonjour ! ceci est un message 4'),
(9, '2023-01-17 17:30:42', 2, 'bonjour ! ceci est un message 5'),
(10, '2023-01-17 17:30:45', 2, 'bonjour ! ceci est un message 6'),
(11, '2023-01-19 13:47:54', 2, 'bonjour ! ceci est un message 7'),
(12, '2023-01-19 13:52:24', 2, 'adsf'),
(13, '2023-01-19 13:52:28', 2, 'adsf2'),
(14, '2023-01-19 13:52:29', 2, 'adsf23'),
(15, '2023-01-19 13:52:29', 2, 'adsf234'),
(16, '2023-01-19 13:52:30', 2, 'adsf2345'),
(17, '2023-01-19 13:52:30', 2, 'adsf23456'),
(18, '2023-01-19 13:52:31', 2, 'adsf234567'),
(19, '2023-01-19 13:52:31', 2, 'adsf2345678'),
(20, '2023-01-19 13:52:35', 2, 'adsf23456789'),
(21, '2023-01-19 13:52:37', 2, 'adsf23456789'),
(22, '2023-01-19 13:55:11', 2, 'asdf'),
(23, '2023-01-19 13:55:24', 4, 'asdf'),
(24, '2023-01-19 13:55:24', 4, 'asdf'),
(25, '2023-01-19 13:55:31', 4, 'asdf2'),
(26, '2023-01-19 13:55:31', 4, 'asdf2'),
(27, '2023-01-19 13:59:18', 2, 'aa'),
(28, '2023-01-19 13:59:27', 4, 'aa'),
(29, '2023-01-19 14:00:04', 4, 'bonjour'),
(30, '2023-01-19 14:00:12', 4, 'au revoir !'),
(31, '2023-01-19 14:00:51', 9, 'bonjour'),
(32, '2023-01-19 14:00:57', 9, 'yes'),
(33, '2023-01-19 14:01:18', 10, 'petit test'),
(34, '2023-01-19 14:03:09', 6, 'lkj'),
(35, '2023-01-19 14:04:50', 14, 'mon message'),
(36, '2023-01-19 14:05:32', 2, 'bonjour'),
(37, '2023-01-19 14:07:10', 15, 'bonjour tout le monde');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `datecreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `datecreated`) VALUES
(1, '2023-01-17 16:31:27'),
(2, '2023-01-17 16:32:47'),
(4, '2023-01-19 10:57:01'),
(5, '2023-01-19 10:57:20'),
(6, '2023-01-19 11:00:49'),
(7, '2023-01-19 11:02:44'),
(8, '2023-01-19 11:05:40'),
(9, '2023-01-19 14:00:36'),
(10, '2023-01-19 14:01:09'),
(11, '2023-01-19 14:01:32'),
(12, '2023-01-19 14:02:19'),
(13, '2023-01-19 14:04:01'),
(14, '2023-01-19 14:04:39'),
(15, '2023-01-19 14:06:59'),
(16, '2023-01-19 14:09:09'),
(17, '2023-01-19 14:09:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_messages_roomid` (`roomid`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `fk_messages_rooms` FOREIGN KEY (`roomid`) REFERENCES `rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
