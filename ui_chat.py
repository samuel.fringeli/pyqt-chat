# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'chat.ui'
##
## Created by: Qt User Interface Compiler version 6.4.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QLineEdit, QPushButton, QSizePolicy,
    QTextBrowser, QWidget)

class Ui_Widget(object):
    def setupUi(self, Widget):
        if not Widget.objectName():
            Widget.setObjectName(u"Widget")
        Widget.resize(600, 520)
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Widget.sizePolicy().hasHeightForWidth())
        Widget.setSizePolicy(sizePolicy)
        Widget.setMaximumSize(QSize(16777215, 16777215))
        Widget.setStyleSheet(u"QTextBrowser {\n"
"    background-color: transparent;\n"
"    border: none;\n"
"}")
        self.chat_title = QTextBrowser(Widget)
        self.chat_title.setObjectName(u"chat_title")
        self.chat_title.setGeometry(QRect(0, -10, 601, 61))
        font = QFont()
        font.setPointSize(48)
        font.setBold(True)
        self.chat_title.setFont(font)
        self.chat_title.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.chat_title.setHorizontalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        self.messages_content = QTextBrowser(Widget)
        self.messages_content.setObjectName(u"messages_content")
        self.messages_content.setGeometry(QRect(10, 70, 581, 371))
        self.input_message = QLineEdit(Widget)
        self.input_message.setObjectName(u"input_message")
        self.input_message.setGeometry(QRect(10, 450, 581, 21))
        self.btn_send_message = QPushButton(Widget)
        self.btn_send_message.setObjectName(u"btn_send_message")
        self.btn_send_message.setGeometry(QRect(490, 480, 100, 32))

        self.retranslateUi(Widget)

        QMetaObject.connectSlotsByName(Widget)
    # setupUi

    def retranslateUi(self, Widget):
        Widget.setWindowTitle(QCoreApplication.translate("Widget", u"Widget", None))
        self.chat_title.setHtml(QCoreApplication.translate("Widget", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><meta charset=\"utf-8\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"hr { height: 1px; border-width: 0; }\n"
"li.unchecked::marker { content: \"\\2610\"; }\n"
"li.checked::marker { content: \"\\2612\"; }\n"
"</style></head><body style=\" font-family:'.AppleSystemUIFont'; font-size:48pt; font-weight:700; font-style:normal;\">\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Chat - salle 98</p></body></html>", None))
        self.messages_content.setHtml(QCoreApplication.translate("Widget", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><meta charset=\"utf-8\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"hr { height: 1px; border-width: 0; }\n"
"li.unchecked::marker { content: \"\\2610\"; }\n"
"li.checked::marker { content: \"\\2612\"; }\n"
"</style></head><body style=\" font-family:'.AppleSystemUIFont'; font-size:13pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Chargement des messages... Veuillez patienter</p></body></html>", None))
        self.input_message.setPlaceholderText(QCoreApplication.translate("Widget", u"Nouveau message", None))
        self.btn_send_message.setText(QCoreApplication.translate("Widget", u"Envoyer", None))
    # retranslateUi

