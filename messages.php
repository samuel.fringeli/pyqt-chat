<?php

if (!isset($_GET['roomId'])) die('room not found');

require "db.php";

$query = $db->prepare('SELECT COUNT(*) FROM rooms WHERE id = :id');
$query->execute(['id' => $_GET['roomId']]);
if (intval($query->fetch()[0]) === 0) die('room not found');

if (isset($_POST['content'])) {
    $db->prepare('INSERT INTO messages(datecreated, roomid, content) VALUES (NOW(), :roomid, :content)')
        ->execute(['roomid' => $_GET['roomId'], 'content' => $_POST['content']]);
    die($db->lastInsertId());
}

$select = $db->prepare('SELECT id, content, datecreated FROM messages WHERE roomid = :roomid ORDER BY datecreated DESC');
$select_params = ['roomid' => $_GET['roomId']];
if (isset($_GET['limit']) && is_numeric($_GET['limit'])) {
    $select = $db->prepare('SELECT id, content, datecreated FROM messages WHERE roomid = :roomid ORDER BY datecreated DESC LIMIT '.$_GET['limit']);
}

$select->execute($select_params);
echo json_encode($select->fetchAll(PDO::FETCH_ASSOC));
