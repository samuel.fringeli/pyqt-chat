<?php

if (!isset($_GET['roomId'])) die('false');

require "db.php";

$query = $db->prepare('SELECT COUNT(*) FROM rooms WHERE id = :id');
$query->execute(['id' => $_GET['roomId']]);
$result = $query->fetch();

die(intval($result[0]) === 0 ? 'false' : 'true');
