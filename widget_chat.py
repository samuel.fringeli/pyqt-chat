# This Python file uses the following encoding: utf-8
import sys

from PySide6.QtCore import QTimer
from PySide6.QtWidgets import QApplication, QWidget, QVBoxLayout, QMessageBox
import ui_chat
import ui_chat_connect
import requests
import json

BASE_URL = "http://localhost/"


def centered_text(s):
    return '<div style="text-align: center;">' + str(s) + '</div>'


class WidgetChat(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = ui_chat.Ui_Widget()
        self.ui.setupUi(self)


class WidgetChatConnect(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = ui_chat_connect.Ui_Widget()
        self.ui.setupUi(self)


class OpenChat(QWidget):
    def __init__(self, app_context):
        super().__init__()
        layout = QVBoxLayout()
        self.app_context = app_context
        self.widgetChat = WidgetChat()
        self.setGeometry(50, 50, self.widgetChat.size().width() + 45, self.widgetChat.size().height() + 30)
        layout.addWidget(self.widgetChat)
        self.setLayout(layout)


class MyApp:
    def __init__(self):
        qapp = QApplication(sys.argv)
        self.previous_messages = 'NO_MESSAGE'
        self.widget = WidgetChatConnect()
        self.open_chat = OpenChat(self)
        self.widget.ui.connect_button.clicked.connect(self.open_chat_window)
        self.widget.ui.connect_button.setEnabled(False)
        self.widget.ui.room_input.textChanged[str].connect(lambda: self.widget.ui.connect_button.setEnabled(self.widget.ui.room_input.text() != ""))
        self.widget.ui.create_room_button.clicked.connect(self.get_new_room)
        self.open_chat.widgetChat.ui.input_message.textChanged[str].connect(lambda: self.open_chat.widgetChat.ui.btn_send_message.setEnabled(self.open_chat.widgetChat.ui.input_message.text() != ""))
        self.open_chat.widgetChat.ui.btn_send_message.clicked.connect(self.send_message)
        self.widget.show()
        sys.exit(qapp.exec())

    def get_new_room(self):
        room_id = (requests.request("GET", BASE_URL + 'new_room.php')).text
        self.widget.ui.room_input.setText(room_id)

    def open_chat_window(self):
        room_id = self.widget.ui.room_input.text()
        room_correct = (requests.request("GET", BASE_URL + 'check_room.php?roomId=' + room_id)).text == 'true'
        if room_correct:
            self.room_id = room_id
            self.open_chat.closeEvent = self.close_chat_event
            self.close_chat_event()
            self.open_chat.widgetChat.ui.btn_send_message.setEnabled(False)
            self.open_chat.widgetChat.ui.chat_title.setText(centered_text('Chat - salle ' + self.room_id))
            self.refresh_messages()
            self.open_chat.show()
            self.chat_loop()
        else:
            QMessageBox.about(self.widget, '', "Cette salle n'existe pas. Veuillez entrer un autre numéro ou créer une salle.")

    def close_chat_event(self, event=None):
        self.open_chat.widgetChat.ui.messages_content.setText('Chargement des messages... Veuillez patienter')
        self.open_chat.widgetChat.ui.input_message.setText('')
        try: self.timer.stop()
        except: pass
        if event: event.accept()

    def chat_loop(self):
        try: self.timer.stop()
        except: pass

        self.timer = QTimer(self.open_chat.widgetChat)
        self.timer.timeout.connect(self.refresh_messages)
        self.timer.start(500)

    def refresh_messages(self):
        messages_res = (requests.request("GET", BASE_URL + 'messages.php?roomId=' + self.room_id)).text
        messages = json.loads(messages_res)
        str_messages = ['<div><span style="color: #6c757d">{}</span> {}'.format(m['datecreated'], m['content']) for m in messages]
        str_all_messages = '<br>'.join(str_messages)
        if str_all_messages != self.previous_messages:
            self.previous_messages = str_all_messages
            self.open_chat.widgetChat.ui.messages_content.setText(str_all_messages)

    def send_message(self):
        payload = {'content': self.open_chat.widgetChat.ui.input_message.text()}
        response = requests.request("POST", BASE_URL + 'messages.php?roomId=' + self.room_id, data=payload)
        if not response.text.isnumeric():
            QMessageBox.about(self.widget, '', "Erreur : " + str(response.text))
        else:
            self.open_chat.widgetChat.ui.input_message.setText('')
            self.refresh_messages()

if __name__ == "__main__":
    MyApp()
