<?php

if (!isset($_GET['roomId'])) die('room not found');

require "db.php";

$query = $db->prepare('SELECT COUNT(*) FROM rooms WHERE id = :id');
$query->execute(['id' => $_GET['roomId']]);
if (intval($query->fetch()[0]) === 0) die('room not found');

$db->prepare('DELETE FROM rooms WHERE id = :id')->execute(['id' => $_GET['roomId']]);
die('ok');
