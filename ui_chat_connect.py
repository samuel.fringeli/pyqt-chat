# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'chat_connect.ui'
##
## Created by: Qt User Interface Compiler version 6.4.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QGridLayout, QLineEdit, QPushButton,
    QSizePolicy, QTextBrowser, QWidget)

class Ui_Widget(object):
    def setupUi(self, Widget):
        if not Widget.objectName():
            Widget.setObjectName(u"Widget")
        Widget.resize(420, 259)
        Widget.setStyleSheet(u"QTextBrowser {\n"
"    background-color: transparent;\n"
"    border: none;\n"
"}")
        self.textBrowser = QTextBrowser(Widget)
        self.textBrowser.setObjectName(u"textBrowser")
        self.textBrowser.setGeometry(QRect(10, 10, 401, 81))
        self.gridLayoutWidget = QWidget(Widget)
        self.gridLayoutWidget.setObjectName(u"gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(0, 90, 421, 151))
        self.gridLayout = QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.room_input = QLineEdit(self.gridLayoutWidget)
        self.room_input.setObjectName(u"room_input")

        self.gridLayout.addWidget(self.room_input, 2, 1, 1, 1)

        self.textBrowser_2 = QTextBrowser(self.gridLayoutWidget)
        self.textBrowser_2.setObjectName(u"textBrowser_2")

        self.gridLayout.addWidget(self.textBrowser_2, 0, 0, 1, 1)

        self.create_room_button = QPushButton(self.gridLayoutWidget)
        self.create_room_button.setObjectName(u"create_room_button")

        self.gridLayout.addWidget(self.create_room_button, 0, 1, 1, 1)

        self.textBrowser_3 = QTextBrowser(self.gridLayoutWidget)
        self.textBrowser_3.setObjectName(u"textBrowser_3")

        self.gridLayout.addWidget(self.textBrowser_3, 0, 2, 1, 1)

        self.connect_button = QPushButton(self.gridLayoutWidget)
        self.connect_button.setObjectName(u"connect_button")

        self.gridLayout.addWidget(self.connect_button, 3, 1, 1, 1)

        self.textBrowser_4 = QTextBrowser(self.gridLayoutWidget)
        self.textBrowser_4.setObjectName(u"textBrowser_4")
        self.textBrowser_4.setMinimumSize(QSize(250, 0))

        self.gridLayout.addWidget(self.textBrowser_4, 1, 1, 1, 1)


        self.retranslateUi(Widget)

        QMetaObject.connectSlotsByName(Widget)
    # setupUi

    def retranslateUi(self, Widget):
        Widget.setWindowTitle(QCoreApplication.translate("Widget", u"Mon super chat", None))
        self.textBrowser.setHtml(QCoreApplication.translate("Widget", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><meta charset=\"utf-8\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"hr { height: 1px; border-width: 0; }\n"
"li.unchecked::marker { content: \"\\2610\"; }\n"
"li.checked::marker { content: \"\\2612\"; }\n"
"</style></head><body style=\" font-family:'.AppleSystemUIFont'; font-size:13pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:48pt; font-weight:700;\">Mon super chat</span></p></body></html>", None))
        self.room_input.setPlaceholderText(QCoreApplication.translate("Widget", u"Num\u00e9ro de la salle", None))
        self.create_room_button.setText(QCoreApplication.translate("Widget", u"Cr\u00e9er une nouvelle salle", None))
        self.connect_button.setText(QCoreApplication.translate("Widget", u"Connexion au chat", None))
        self.textBrowser_4.setHtml(QCoreApplication.translate("Widget", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><meta charset=\"utf-8\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"hr { height: 1px; border-width: 0; }\n"
"li.unchecked::marker { content: \"\\2610\"; }\n"
"li.checked::marker { content: \"\\2612\"; }\n"
"</style></head><body style=\" font-family:'.AppleSystemUIFont'; font-size:13pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:18pt;\">ou entrer le num\u00e9ro de la salle</span></p></body></html>", None))
    # retranslateUi

